/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.ejawa.bean.html.ValueHtmlAsLinks;
import org.ejawa.bean.values.EString;
import org.ejawa.util.EasyHtml;
import org.ejawa.util.EasyLog;
import org.ejawa.util.EasyString;


/**
 * @author david jeanneteau
 */
public abstract class EasyBean {
	// Class attributes
	private static EasyLog logger=new EasyLog(EasyBean.class);
	
	// Http session attribute name 
	protected static String WA_ATTRIB_EJAWA="ejawa_bean";
	
	// Attributs d'instance (pour chaque client connecte)
	protected EasyValueSet values=null;

	protected boolean isDefaultActionsEnabled;
	protected boolean isDisplayBeanEnabled;
	//public EBoolean displayBean=new EBoolean(DEFAULT_ACTION_DISPLAY_BEAN);
	
	private EString defaultAction;
	public String DEFAULT_ACTION_RESET="resetSession";
	public String DEFAULT_ACTION_DISPLAY_BEAN="displayBean";
	public String DEFAULT_ACTION_DISPLAY_LOGS="displayLogsTODO";
	public String DEFAULT_ACTION_MANAGE_CONFIG="manageConfigurationTODO";
	
	// Constructors
	/**
	 * Protected constructor
	 * use getBean(HttpServletRequest) to get a bean instance
	 */
	protected EasyBean() {
		values=initValues();
		if (isDefaultActionsEnabled) {
			defaultAction=new EString("ejawaAction");
			defaultAction.addDefaultValue(DEFAULT_ACTION_RESET);
			defaultAction.addDefaultValue(DEFAULT_ACTION_DISPLAY_BEAN);
			defaultAction.addDefaultValue(DEFAULT_ACTION_DISPLAY_LOGS);
			defaultAction.addDefaultValue(DEFAULT_ACTION_MANAGE_CONFIG);
			defaultAction.setHtmlMaker(
					new ValueHtmlAsLinks());
		}
	}
	
	// Abstract methods
	/**
	 * Init beanValues
	 */
	protected abstract EasyValueSet initValues() ;
	protected abstract void doActions(HttpServletRequest request) ;
	
	// Helpers
	public EasyString getValue(String name) {
		return values.getValue(name);
	}
	
	public String makeHtmlDebugActions() {
		String l_ret="<div>";
		if (isDefaultActionsEnabled) {
			l_ret+=defaultAction.makeHtml()+"<br/>";
		} else {
			logger.debug("default actions not enabled : not displayed");
		} 
		
		if (isDisplayBeanEnabled) {
			l_ret+="display bean:";
			l_ret+=EasyHtml.textToHtml(values.makeDebugDisplay("- "), true);
			l_ret+="<br/>";
		}
		l_ret+="</div>";
		return l_ret;
	}
	
	// Methods
	public void processRequest(HttpServletRequest request) {
		logger.debug( "processRequest()");

		// Update bean valus
		logger.debug( "WebBean.processRequest() - update bean");
		update(request);
		
		// process default actions
		if (isDefaultActionsEnabled) {
			logger.debug( "WebBean.processRequest() - process default actions");
			defaultAction.updateRawValue(request);
			doDefaultActions(request);
		}
		
		// process sub class actions
		doActions(request);
		
		logger.debug(  "processRequest(): end");
	}
	

	private void doDefaultActions(HttpServletRequest request) {
		if(defaultAction.hasValue(DEFAULT_ACTION_RESET)) {
			actionResetSession(request);
		} else  if(defaultAction.hasValue(DEFAULT_ACTION_DISPLAY_BEAN)) {
			actionDisplayBean();
		}
		defaultAction.setValue("");
	}

	private void actionResetSession(HttpServletRequest request) {
		logger.debug("actionResetSession()");
		HttpSession l_session = request.getSession();
		l_session.removeAttribute(WA_ATTRIB_EJAWA);
	}

	private void actionDisplayBean() {
		logger.debug("actionDisplayBean");
		isDisplayBeanEnabled=!isDisplayBeanEnabled;
	}

	public static EasyBean getBean(HttpServletRequest request, Class beanClazz) {	
		// Get bean from servlet's session
		HttpSession l_session = request.getSession();
		EasyBean l_bean=(EasyBean) l_session.getAttribute(WA_ATTRIB_EJAWA);
		if (l_bean==null) {
			// Create new instance of bean 
 			try {
				l_bean=(EasyBean) beanClazz.newInstance();
			} catch (Exception e) {
				logger.error("WebBean.getBean(): cannot create new instance of ["
						+beanClazz.getName()+"]");
				e.printStackTrace();
			}
			l_session.setAttribute(WA_ATTRIB_EJAWA,l_bean);
			logger.info("created new session bean: ["
					+beanClazz.getName()+"]");			
			}

		return l_bean;	
	}
	
	public void update(HttpServletRequest request) {	
		// Update values
		values.updateRawValue(request);
	}
}
