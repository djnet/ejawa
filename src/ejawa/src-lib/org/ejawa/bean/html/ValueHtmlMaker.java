/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.html;

import java.util.Iterator;
import java.util.Vector;

import org.ejawa.bean.EasyValue;
import org.ejawa.util.EasyHtml;

/**
 * @author isa
 *
 */
// TODO: make an interface instead of a class ?
public class ValueHtmlMaker {
	int nbDefaultValuesPerRow=4;
	
	public String makeHtml(EasyValue value) {
		String l_ret=( value.isDisplayLabel() ? "	"+value.getLabel()
					+makeHtmlFieldReset(value)+"<br/>\n" 
					: "" )
				+"	"+makeHtmlInput(value)+"\n"
				+makeHtmlDefaultValueButtons(value);
		return l_ret;
	}
	
	private String makeHtmlFieldReset(EasyValue value) {
		StringBuffer l_ret=new StringBuffer("");
		if (value.isDisplayResetButton()) {
			l_ret.append("    <input value='reset' type='button' onmousedown='form.");
			l_ret.append(value.getName());		 
			l_ret.append(".value=\"\"' />\n");
//		 	link reset : do not work :(
//			l_ret.append("    <a href='' onmousedown='form.");
//			l_ret.append(value.getName());		// http param
//			l_ret.append(".value=\"\"'>reset</a>\n");
		}
		return l_ret.toString();
	}
	
	public String makeHtmlDefaultValueButtons(EasyValue value) {
		StringBuffer l_ret=new StringBuffer("");
		int l_nbInRow=0;
		Vector defaultValues=value.getDefaultValues();
		if ( defaultValues!=null ) {
			for (Iterator i=defaultValues.iterator(); i.hasNext();) {
				if ( ++l_nbInRow>nbDefaultValuesPerRow ) {
					l_ret.append("  <br/>\n");
					l_nbInRow=1;
				}
				String l_defaultValue[]=(String[]) i.next();
				String l_label=l_defaultValue[1];
				String l_value=l_defaultValue[0];
				l_ret.append("    <input type='button' ");
				l_ret.append("value='"+l_label+"'");
				l_ret.append("' onmousedown='form."
						+value.getName()+".value=\"");
				l_ret.append(l_value);	
				l_ret.append("\"' />\n");
			}
		}
		return l_ret.toString();
	}
	
	public static String makeHtmlInput(EasyValue value) {
		return EasyHtml.makeHtmlInput("", 
				value.getName(), value.getRawValue().asStringNotNullOrEmpty());
	}


}
