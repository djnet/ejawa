/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.html;

/**
 * @author dj
 * 
 * Cette classe genere le code html pour une liste d'éléments.
 * 
 * Elle donne les éléments :
 * - header (avant le premier élément) {@link #getHeader()}
 * - le separateur (entre plusieurs éléments) {@link #getSepSkipFirst()}
 * - le footer (après le dernier élément: {@link #getFooter()}
 *
 */
public class HtmlSetFactory {
	private String header;
	private String footer;
	private String evenSep;
	private String oddSep;
	private int count;
	
	public HtmlSetFactory(String header, String footer, String evenSep,
			String oddSep) {
		super();
		this.header = header;
		this.footer = footer;
		this.evenSep = evenSep;
		this.oddSep = oddSep;
	}
	
	public HtmlSetFactory(String header, String footer, String sep) {
		super();
		this.header = header;
		this.footer = footer;
		this.evenSep = sep;
		this.oddSep = sep;
	}

	public String getHeader() {
		count=0;
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public void setEvenSep(String evenSep) {
		this.evenSep = evenSep;
	}

	public String getSepSkipFirst() {
		String l_ret="";
		if (count++>0) {
			l_ret= count%2 == 0 ? oddSep : evenSep ;
		}
		return l_ret;
	}

	public void setOddSep(String oddSep) {
		this.oddSep = oddSep;
	}

	public void setSep(String sep) {
		setOddSep(sep);
		setEvenSep(sep);
	}
	
	
}
