/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.html;

import java.util.Iterator;

import org.ejawa.bean.EasyValue;
import org.ejawa.util.EasyHtml;

/**
 * @author david jeanneteau
 * Display value as links (one link per default value)
 * 
 */
public class ValueHtmlAsLinks extends ValueHtmlMaker {

	@Override
	public String makeHtml(EasyValue value) {
		String l_ret="";
		String l_name=value.getName();
		String l_sep="";
		Iterator<String[]> i=value.getDefaultValues().iterator(); 
		while( i.hasNext()) {
			String[] l_defaultValue=i.next();
			String l_value=l_defaultValue[0];
			String l_label=l_defaultValue[1];
			l_ret+=l_sep + EasyHtml.makeHtmlLink(
								"", l_name, l_value, l_label);
			l_sep=" - ";
		}
		
		return l_ret;
	}

	@Override
	public String makeHtmlDefaultValueButtons(EasyValue value) {
		return "";
	}
	
}
