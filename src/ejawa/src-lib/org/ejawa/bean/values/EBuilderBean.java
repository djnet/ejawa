/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.values;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.ejawa.bean.EasyValue;
import org.ejawa.bean.EasyValueSet;

/**
 * @author djnet
 * 
 * Constructeur de bean (dynamique)
 *
 */
public class EBuilderBean extends EasyValue {
	EString beanClass;
	EString beanName;
	EasyValueSet beanParameters;
	
	public enum ValueTypeEnum {
		ESTRING(EString.class, "Champs texte", "Champs texte"),
		EINT(EInt.class, "Entier", "Entier"),
		ESET(EasyValueSet.class, "Liste", "Liste de valeurs");
		
		public Class clazz;
		public String desc;
		public String label;

		ValueTypeEnum( Class clazz, String label, String desc ) {
			this.clazz=clazz;
			this.label=label;
			this.desc=desc;
		}
	};

	public EBuilderBean(String name, String label) {
		super(name, label);
	}

	public EBuilderBean(String name) {
		super(name);
	}

	protected void init(String name, String label) {
		super.init(name, label);
		beanClass=new EString("class", "Type de valeur");
		List<ValueTypeEnum> types=Arrays.asList( ValueTypeEnum.values() );
		for (ValueTypeEnum type : types) {
			beanClass.addDefaultValue( type.label );
		}
		beanName=new EString("Nom");
		beanParameters=new EasyValueSet("Paramètres","Paramètres");
	}
	
	public void update() {
		// Nothing to do here ...
	}


	/**
	 * @deprecated Use {@link #makeHtml()} instead
	 */
	public String makeHtmlInput() {
		return makeHtml();
	}


	public String makeHtml() {
		// TODO Auto-generated method stub
		return super.makeHtml();
	}
	
	
}
