/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.values;

import java.util.Vector;

import org.ejawa.bean.EasyValue;
import org.ejawa.bean.html.ValueHtmlAsLinks;

public class EBoolean extends EasyValue {
	private boolean value;

	public EBoolean(String name, String label, String onLabel, String offLabel) {
		super(name, label);
		setOnOffLabels(onLabel, offLabel);
	}

	public EBoolean(String name, String label) {
		super(name, label);
	}

	public EBoolean(String name) {
		super(name, name);
	}

	protected void init(String name, String label) {
		super.init(name, label);
		value=false;
	}
	
	public void setOnOffLabels(String onLabel, String offLabel) {
		defaultValues=new Vector<String[]>();
		addDefaultValue("true", onLabel);
		addDefaultValue("false", offLabel);
		htmlMaker=new ValueHtmlAsLinks();
	}

	public void update() {
		setValue(rawValue.isTrue());
	}

	public boolean value() {
		return value;
	}

	public void setValue(boolean newValue) {
		value=newValue;

		//updates rawValue
		rawValue.setValue(""+value);
	}
	
}
