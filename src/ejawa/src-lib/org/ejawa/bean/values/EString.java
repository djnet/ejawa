/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.values;

import org.ejawa.bean.EasyValue;

public class EString extends EasyValue {
	
	public EString(String name, String label) {
		super(name, label);
	}

	public EString(String name) {
		super(name);
	}

	public String value() {
		return rawValue.asString();
	}

	public void update() {
		// Nothing to do: use rawValue directly 
	}
	
	public void setValue(String val) {
		rawValue.setValue(val);
	}

	/**
	 * Compare current value to given compareValue
	 * 
	 * @param compareValue
	 * @return true is compareValue non null and equals current value 
	 */
	public boolean hasValue(String compareValue) {
		boolean l_ret=false;
		if (value() != null) {
			l_ret=value().equals(compareValue);
		}
		return l_ret;
	}
}
