/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.values;

import org.ejawa.bean.EasyValue;

public class EInt extends EasyValue {
	private int max;
	private int min;
	private int value;

	public EInt(String name, String label,int min, int max) {
		super(name, label);
		this.min=min;
		setValue(min);
		this.max=max;
	}

	public EInt(String name, String label) {
		super(name, label);
	}

	public EInt(String name) {
		super(name, name);
	}

	protected void init(String name, String label) {
		super.init(name, label);
		min=0;
		value=0;
		max=-1;
	}

	public void update() {
		setValue(rawValue.asInt(min));
	}

	public int value() {
		return value;
	}

	public void setValue(int newValue) {
		value=newValue;

		// Check limits if min<max
		if (min<max) {
			if (newValue<min) {
				value=min;
			} else if (newValue>max) {
				value=max;
			}
		}
		
		//updates rawValue
		rawValue.setValue(""+value);
	}

}
