/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean.values;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.ejawa.bean.EasyValueSet;
import org.ejawa.bean.html.HtmlSetFactory;

public class EDate extends EasyValueSet {
	EInt jour;
	EInt mois;
	EInt annee;
	GregorianCalendar date;

	public EDate(String name, String label) {
		super(name, label);
	}


	public EDate(String name) {
		super(name, name);
	}

	protected void init(String name, String label) {
		super.init(name, label);
		date=new GregorianCalendar();
		jour=new EInt(name+".jj","Jour", 1, 31);
		mois=new EInt(name+".mm","Mois", 1, 12);
		annee=new EInt(name+".yy","Année", 2000, 2050);
		annee.setValue(new Date().getYear());
		addValue(jour);
		addValue(mois);
		addValue(annee);
		htmlSetFactory.setHeader("<div>");
		htmlSetFactory.setSep("&nbsp;");
		htmlSetFactory.setFooter("</div>");
//		htmlSetFactory.setHeader("<table><tr><td>");
//		htmlSetFactory.setSep("</td><td>");
//		htmlSetFactory.setFooter("</td></tr></table>");
	}
	
	public Date value() {
		date.set(annee.value(), mois.value(), jour.value() );
		return date.getTime();
	}
	
	public void setDate(Date date) {
		// TODO use not deprecated methods (Calendar class)
		jour.setValue(date.getDay());
		mois.setValue(date.getMonth());
		annee.setValue(date.getYear());
	}


	public void update() {
		// Nothing to do here ...
	}


	public String makeHtml() {
		return super.makeHtml();
	}
	
	
}
