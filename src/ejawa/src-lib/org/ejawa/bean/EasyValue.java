/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean;

//TODO: add EValues uses a EasyValueHtmlView (
//TODO: EDate can display itself (date picker)


import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.ejawa.bean.html.ValueHtmlMaker;
import org.ejawa.util.EasyLog;
import org.ejawa.util.EasyString;

/**
 * @author david jeanneteau
 *
 * EasyValue is a base for handling html inputs.
 * 
 * It stores a rawValue (updated from http request parameters).
 * rawValue will be converted into usefull value (date, number ...) 
 * in sub classes.
 * 
 * An EasyValue instance can display itself in HTML.
 * 
 * It can use default values 
 * (to be used as combox, radiobutton or default values buttons ...)
 *  
 */
public abstract class EasyValue {
	// Class attributes
	private static EasyLog logger=new EasyLog(EasyValue.class);
	private static ValueHtmlMaker defaultHtmlMaker;
	
	
	//ViewMaker viewMaker=null();
	
	/**
	 * Value set by html request
	 */
	protected EasyString rawValue;
	protected String label;
	protected String name;
	protected Vector<String[]> defaultValues;

	protected boolean displayLabel;
	protected boolean displayResetButton;
	
	
	protected ValueHtmlMaker htmlMaker;
	/**
	 * If enabled, new value will be added to list of default values 
	 */
	private boolean defaultValueAutoUpdate;
	
	protected boolean displayBigInput;
	
	protected boolean debugMode;

	static{
		defaultHtmlMaker=new ValueHtmlMaker();	
	}
	
	public EasyValue(String name) {
		init (name, name);
	}

	public EasyValue(String name, String label) {
		init(name, label);
	}

	protected void init(String name, String label) {
		if (name==null) {
			logger.error("EasyValue name=null !!!");
			name="null";
		}
		this.name=name;
		this.label=label;
		displayLabel=true;
		displayResetButton=false;
		displayBigInput=false;

		defaultValues=new Vector<String[]>();
		defaultValueAutoUpdate=false;
		
		debugMode=false;
		
		rawValue=new EasyString(null);
		htmlMaker=getDefaultvalueHtmlMaker();
	}

	public String makeHtml() {
		String l_ret=null;
		l_ret=htmlMaker.makeHtml(this);
		return l_ret;
	}

	public String makeDebugDisplay(String indent) {
		// TODO should be overriden from 'EasyValue' (and renamed 'toString()' ??)
		
		String l_ret=indent;
		l_ret+=getClass().getName()+" name:'"+name+"', label='"+label+"'," 
				+" value='"+rawValue.asString()+"'";
		return l_ret;
	}

	/**
	 * Update rawValue using request parameter
	 * 
	 * @param request
	 * @return true if the parameter was set on the request (rawValue was updated) 
	 */
	public boolean updateRawValue(HttpServletRequest request) {
		boolean l_ret=false;
		String l_param=request.getParameter(name);
		if (l_param!=null) {
			l_ret=true;
			setRawValue(l_param);
			update();
			if (debugMode) {
				logger.debug( "EasyValue["+name+"]='"+l_param+"'");
			}
			
			// Add new value to default values, if expected by this EasyValue 
			if (defaultValueAutoUpdate) {
				if ( !hasDefaultValue(l_param)) {
					addDefaultValue(l_param);
				}
			}
		}
		return l_ret;
	}

	/**
	 * This method update effective value (may be from rawValue)
	 */
	public abstract void update();

	public EasyString getRawValue() {
		return rawValue;
	}

	public void setRawValue(String value) {
		// TODO: add a flag to keep trace of rawValue change
		// this flag may be used in update() method
		rawValue.setValue(value);
	}
	
	// --- default values -----------
	/**
	 * Check if value is already a defaultValue
	 *  
	 * @param value 
	 * @return true if value already is a defaultValue 
	 */
	public boolean hasDefaultValue(String value) {
		boolean l_ret=false;
		if ( defaultValues!=null && value !=null ) {
			for (Iterator i=defaultValues.iterator(); i.hasNext();) {
				String l_item[]=(String[]) i.next();
				if ( value.equals(l_item[0]) ) {
					l_ret=true;
				}
			}
		}
		return l_ret;
	}
	
	public void addDefaultValue(String defaultValue,String label) {
		String l_defaultValue[]={defaultValue,label};
		if (!hasDefaultValue(defaultValue)) {
			defaultValues.add(l_defaultValue);
		}
		
		// Set initial value
		if (rawValue.isNull()) {
			setRawValue(defaultValue);
		}
	}
	
	public void setDefaultValues(String[] values) {
		defaultValues.removeAllElements();
		for (int i=0; i<values.length; i++) {
			addDefaultValue(values[i]);
		}
	}

	public void addDefaultValue(String value) {
		addDefaultValue(value,value);
	}
	
	public void setDefaultValueAutoUpdate () {
		this.defaultValueAutoUpdate = true;	
	}

	public Vector getDefaultValues() {
		return defaultValues;
	}

	public String getLabel() {
		return label;
	}

	public String getName() {
		return name;
	}

	public boolean isDisplayBigInput() {
		return displayBigInput;
	}

	public boolean isDefaultValueAutoUpdate() {
		return defaultValueAutoUpdate;
	}

	public boolean isDisplayLabel() {
		return displayLabel;
	}

	public boolean isDisplayResetButton() {
		return displayResetButton;
	}

	public void setDisplayResetButton(boolean displayResetButton) {
		this.displayResetButton = displayResetButton;
	}

	public static ValueHtmlMaker getDefaultvalueHtmlMaker() {
		return defaultHtmlMaker;
	}

	public static void setDefaultHtmlMaker(
			ValueHtmlMaker defaultvalueHtmlMaker) {
		EasyValue.defaultHtmlMaker = defaultvalueHtmlMaker;
	}

	public void setHtmlMaker(ValueHtmlMaker htmlMaker) {
		this.htmlMaker = htmlMaker;
	}
}
