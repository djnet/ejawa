/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean;

import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.ejawa.bean.html.HtmlSetFactory;
import org.ejawa.bean.values.EString;
import org.ejawa.util.EasyHtml;
import org.ejawa.util.EasyLog;
import org.ejawa.util.EasyString;
// TODO: EasyNavigationSet -> page a afficher ?
// TODO: Composant de login
// TODO: composant de compte rendu d'action
/**
 * EasyValues stores a set of EasyValue 
 * @see EasyValue
 */
public class EasyValueSet extends EasyValue {
	// Class attributes
	private static EasyLog logger=new EasyLog(EasyValueSet.class);
	
	protected HtmlSetFactory htmlSetFactory;
	
	// Instance fields
	Hashtable values ;
	EasyValue currentValue;

	public EasyValueSet(String name, String label) {
		super(name, label);
	}

	public EasyValueSet(String name) {
		super(name);
	}


	protected void init(String name, String label) {
		super.init(name, label);
		values=new Hashtable();
		htmlSetFactory=new HtmlSetFactory("<div>","</div>","</div><div>");
	}
	
	public EasyValue addValue(String name) {
		return addValue(new EString(name));
	}
	
	public EasyValue addValue(EasyValue easyValue) {
		String l_name=easyValue.getName();
		
		if (l_name==null) {
			logger.error("addValue(EasyValue: "+easyValue.getClass().getName()+") -> name=null !");
		} else if (useValue(l_name)==null) {
			currentValue=easyValue;
			values.put(l_name, easyValue);
		}
		
		return currentValue;
	}
	
	public EasyValue useValue(String name) {
		currentValue=null;
		if ( values==null ) {
			logger.error("useValue():  Values==null for name: '"+name+"'");
		} else if (values.containsKey(name)) {
			currentValue=(EasyValue) values.get(name);
		} else {
			//logger.error("useValue(): unknown Value: '"+name+"'");
		}
		return currentValue;
	}
	
	public void addDefaultValue(String defaultValue) {
		addDefaultValue(defaultValue,defaultValue);
	}
	
	public void addDefaultValue(String[] defaultValues) {
		for (int i=0; i<defaultValues.length;i++) {
			addDefaultValue(defaultValues[i],defaultValues[i]);
		}
	}
	
	
	public void addDefaultValue(String defaultValue, String label) {
		if ( currentValue != null ) {
			if ( !currentValue.hasDefaultValue(defaultValue)) {
				currentValue.addDefaultValue(defaultValue, label);
			}
		}
	}
	public void setDefaultValues(String[] values) {
		if ( currentValue != null ) {
			currentValue.setDefaultValues(values);
		}
	}	

	public EasyString getValue(String name) {
		EasyString l_ret=null;
		useValue(name);
		if ( currentValue != null ) {
			l_ret=currentValue.getRawValue();
		} else {
			l_ret=new EasyString(null);
		}
		return l_ret;
	}
	
	/* (non-Javadoc)
	 * Updates sub-values if, either:
	 * - 'name' is null 
	 * - equals '*'
	 * - 'name' is a parameter of the request (any value)
	 *  
	 * @see org.ejawa.bean.EasyValue#updateRawValue(javax.servlet.http.HttpServletRequest)
	 */
	public boolean updateRawValue(HttpServletRequest request) {
		boolean l_ret=true;
		
		// checks if this value set should be updated
		if (name !=null && !name.equals("*")) {
			// if 'name' is set, updates values 
			l_ret=super.updateRawValue(request);
		}
		if (l_ret) {
			for (Iterator i=values.values().iterator(); i.hasNext(); ){
				EasyValue l_value = (EasyValue) i.next();
				
				l_value.updateRawValue(request);
			}
		}
		return l_ret;
	}
	
	public String makeHtmlForm() {
		String l_ret="<form method='get' action=''>";
		l_ret+=makeHtml();
		l_ret+="<input type='submit'>";
		l_ret+="</form>";
		return l_ret;
	}
	
	@Override
	public String makeHtml() {
		String l_ret=htmlSetFactory.getHeader();
		for (Iterator i=values.values().iterator(); i.hasNext(); ){
			l_ret+=htmlSetFactory.getSepSkipFirst();
			EasyValue l_value = (EasyValue) i.next();
			l_ret +=l_value.makeHtml();
		}
		l_ret+=htmlSetFactory.getFooter();
		return l_ret;
	}


	/**
	 * @param name Name of EasyValue
	 * @return String "name=value"
	 */
	public String makeHttpParameter(String name) {
		String l_value=getValue(name).asString();
		return EasyHtml.makeHttpParameterWithUTF8Value(name, l_value);
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	public void setHtmlSetFactory(HtmlSetFactory htmlSetFactory) {
		this.htmlSetFactory = htmlSetFactory;
	}

	/**
	 * @return text that display internal state, for debug purpose
	 */
	public String makeDebugDisplay(String indent) {
		// TODO should be overriden from 'EasyValue' (and renamed 'toString()' ??)
		
		String l_ret=indent;
		l_ret+=getClass().getName()+" name='"+name+"' label='"+label+"'\n";
		for (Iterator i=values.values().iterator(); i.hasNext(); ){
			EasyValue l_value = (EasyValue) i.next();
			
			l_ret +=l_value.makeDebugDisplay(indent+"- ")+"\n";
		}
		return l_ret;
	}
}
