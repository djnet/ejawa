/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.bean;

import java.util.Date;
import java.util.GregorianCalendar;


public class EasyValueNonEmptyOrNull extends EasyValue {

	public EasyValueNonEmptyOrNull(String name, String Name) {
		super(name, name);
	}

	public EasyValueNonEmptyOrNull(String name) {
		super(name);
	}

	protected void init(String name, String label) {
		super.init(name, label);
		setDisplayResetButton(true);
	}
	
	public String getValue() {
		String l_ret=rawValue.asStringNotEmptyOrNull();
	
		return l_ret;
	}

	public String getInitFormValue() {
		String l_value=rawValue.asStringNotNullOrEmpty();
		return (l_value==null ) ? "" : l_value ;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
}
