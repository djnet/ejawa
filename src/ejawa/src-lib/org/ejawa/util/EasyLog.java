/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Formol is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author david.jeanneteau
 * This class log events using a context tag.
 */
public class EasyLog  {
	protected static final String[] logLevels  = 
	{ "ERROR","LOG", "WARN", "INFO","DEBUG"};

	public static final int ERROR = 0;
	public static final int LOG = 1;
	public static final int WARN = 2;
	public static final int INFO = 3;
	public static final int DEBUG = 4;

	private SimpleDateFormat dateFormat=new SimpleDateFormat();
	private int level=DEBUG;
	
	protected String loggerContext;

	public EasyLog(String name) {
		loggerContext=name;
	}

	public EasyLog(Class clazz) {
		loggerContext=clazz.getName();
	}

	private void log(int mode, String msg) {
		if (mode <= level) {
			String l_date=dateFormat.format(new Date());
			String l_message =
				logLevels[mode]
				+" ["
				+l_date
				+"]"
				+" "+(loggerContext!= null && loggerContext.length()>0 
					? loggerContext+"." : "") + msg;
			System.out.println( l_message);
		}
	}

	private void output(String level, String message) {
		System.out.println(level+" "+message);
	}

	// Instance methods
	public void debug(String msg) {
		
		log(DEBUG, msg);
	}
	public void info(String msg) {
		log(INFO, msg);
	}

	public void error(String msg) {
		log(ERROR, msg);	}

	public static String getStackTrace() {
		StringWriter l_buf = new StringWriter();
		try {
			throw new Exception("EasyLog: dump call stack");
		} catch (Exception e) {
			e.printStackTrace(new PrintWriter(l_buf));
		}
		return l_buf.toString();
	}

	public void printStackTrace() {
		debug("dump call stack:");
		debug(getStackTrace());
	}

}
