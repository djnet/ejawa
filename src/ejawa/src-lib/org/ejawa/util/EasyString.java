/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Formol is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * String easy handling
 * 
 * Static methods handles and convert String arrays and list
 */
public class EasyString {
	// Class attributes
	private static final EasyLog logger=new EasyLog(EasyString.class);
	
	// Instance
	private String stringValue;
	
	// Class members
	public static String[] asArray( Collection list) {
		String[] l_ret=new String[list.size()];
		int j=0;
		for (Iterator i=list.iterator(); i.hasNext(); j++) {
			Object l_o=i.next();
			String l_name=null;

			try {
				l_name = (String)l_o;
			} catch (RuntimeException e) {
				logger.error(".asArray(): cast to String, failed for: "
						+l_o);
			}
			l_ret[j]=l_name;
		}
		return l_ret ;
	}
	
	public static Vector asList( String[] array) {
		Vector l_ret=new Vector(Arrays.asList(array));
		return l_ret;
	}
	
	public static String join(List list, String sep) {
		return EasyString.join( EasyString.asArray(list), sep );
	}

	public static String join(String[] array, String sep) {
		StringBuffer l_ret=new StringBuffer("");
		String l_sep="";
		for (int i=0; i< array.length; i++) {
			l_ret.append(l_sep);
			l_sep=sep;
			l_ret.append(array[i]);
		}
		return l_ret.toString();
	}

	// Instance members
	public EasyString(String value) {
		this.stringValue=value;
	}

	public Boolean asBoolean() {
		Boolean l_ret=null;
		if ( stringValue!=null ) {
			l_ret = Boolean.valueOf(stringValue);
		}
		return l_ret;
	}
	
	public Double asDouble() {
		Double l_ret=null;
		if ( stringValue!=null ) {
			l_ret=new Double(stringValue);
		}
		return l_ret;
	}

	public int asInt(int defaultValue) {
		int l_ret=defaultValue;
		try {
			l_ret=asInteger().intValue();
		} catch (Exception e) {
			//
		}
		return l_ret;
	}

	public Integer asInteger() {
		Integer l_ret=null;
		if ( stringValue!=null ) {
			l_ret=new Integer(stringValue);
		}
		return l_ret;
	}

	public String asString() {
		return stringValue;
	}

	public String[] asStringList( String sep) {
		return (stringValue!=null) ? stringValue.split(sep) : new String[0] ;
	}

	public String asStringNotEmptyOrNull() {
		String l_ret=null;
		if(stringValue!=null && stringValue.length()>0) {
			l_ret=stringValue;
		}
		return l_ret;
	}

	public String asStringNotNullOrEmpty() {
		String l_ret=stringValue != null ? stringValue : "";
		return l_ret;
	}

	public EasyString asValueFromStringList( String sep, int index) {
		String l_ret=null;
		if (stringValue!=null) {
			String[] l_list=stringValue.split(sep);
			l_ret = ( index>=0 && index<l_list.length ) 
						? l_list[index] 
				         : null;
		}
		return new EasyString(l_ret);
	}
	
	public void setValue(String value) {
		this.stringValue=value;
	}

	public boolean isNull() {
		return stringValue==null;
	}

	public boolean isTrue() {
		Boolean l_ret=asBoolean();
		return l_ret!=null && l_ret.booleanValue();
	}
}
