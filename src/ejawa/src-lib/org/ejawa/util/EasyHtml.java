/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Formol is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.ejawa.bean.EasyValue;

public class EasyHtml {
	// Class attributes
	private static EasyLog logger=new EasyLog(EasyHtml.class);

	public static String textToHtml(String text, boolean encodeTags) {
		String l_ret=text;
		if (l_ret!=null ) {
			if (encodeTags ) {
				l_ret=l_ret.replaceAll("<","&lt;");
			}
			l_ret=l_ret.replaceAll("[\f\n]","<br/>\n");
		}
		return l_ret;
	}
	
	public static String makeHtmlInput(String inputAttrs, String name, String value) {
		String l_input="<input name='"
			+name+"' value='"+value+"' "
			+inputAttrs+" />";
		return l_input;
	}

	public static String makeHtmlLink(String url, String name, String otherValue, String label) {
		String l_ret="<a href='"+url+"?"
			+name+"="+(otherValue)+"'>"+label+"</a>";
		return l_ret;
	}
	
	public String makeJavaScriptLib() {
		StringBuffer l_ret=new StringBuffer("");
		l_ret. append("<script language=\"JavaScript\">\n");
		l_ret. append("\n");
		l_ret. append("</script>\n");
		return l_ret.toString();
	}
	
	/**
	 * @param name Name of parameter
	 * @param value Value of parameter
	 * @return String "name=value" with value UTF8 encoded 
	 */
	public static String makeHttpParameterWithUTF8Value(String name, String value) {
		String l_ret=null;

		try {
			if (value!=null) {
				l_ret=name+"="+URLEncoder.encode(value,"UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Error encoding url: '"+value+"'");
			e.printStackTrace();
		}
		return l_ret;

	}
}
