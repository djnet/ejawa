/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class EasyConfiguration {
	protected static String WA_ATTRIBUTE_CONFIG="ejawa_configuration";
	
	public static EasyConfiguration getConfiguration(HttpServletRequest request) {
		EasyConfiguration l_ret=null;
		ServletContext l_ctx = request.getSession().getServletContext();
		
		// Try to load configuration instance from webapp's context
		l_ret=(EasyConfiguration) l_ctx.getAttribute(WA_ATTRIBUTE_CONFIG);
		
		if (l_ret==null) {
			// Create config if needed
			l_ret=new EasyConfiguration();
			l_ctx.setAttribute(WA_ATTRIBUTE_CONFIG, l_ret);
		}
		return l_ret;
	}
	
	
}
