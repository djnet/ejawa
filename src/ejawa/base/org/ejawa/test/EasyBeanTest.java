/*
 * Copyright 2008, David Jeanneteau
 *
 *	This file is part of Ejawa (Easy JAva Web Application) .
 *
 * Ejawa is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ejawa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.ejawa.test;
import javax.servlet.http.HttpServletRequest;

import org.ejawa.bean.EasyBean;
import org.ejawa.bean.EasyValue;
import org.ejawa.bean.EasyValueSet;
import org.ejawa.util.EasyLog;


public class EasyBeanTest extends EasyBean {
	public EasyValueSetTest values;

	public static EasyBeanTest getMyBean(HttpServletRequest request) {
		return (EasyBeanTest) getBean(request, EasyBeanTest.class);
	}
	
	@Override
	protected EasyValueSet initValues() {
		values=new EasyValueSetTest("test","EasyBeanTest");
		values.addValue(new EasyValueSet("subset", "subset"));
		isDefaultActionsEnabled=true;

		return values;
	}

	@Override
	protected void doActions(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
	}

}
