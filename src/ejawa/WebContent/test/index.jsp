<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page import="org.ejawa.test.*" %>
<%@ page language="java"%>

<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1 
response.setHeader("Pragma","no-cache"); //HTTP 1.0 
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<head>
  <title>ejawa test page</title>
</head>

<body>
<form action=""></form>
<hr/>
<%
//Gestion du bean a partir de la requete
EasyBeanTest bean=EasyBeanTest.getMyBean(request);
bean.processRequest(request);
%>
<%=bean.makeHtmlDebugActions() %><br/>
<%=bean.values.makeHtmlForm()%>